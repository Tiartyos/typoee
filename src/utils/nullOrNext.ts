import {Nullable} from '../../utils/utility-types';

export const nullOrNext = <A>(a: A, b: A, c: A): Nullable<A> => {
    if (a !== null) return a;
    if (b !== null) return b;
    if (c !== null) return c;

    return null;
};
