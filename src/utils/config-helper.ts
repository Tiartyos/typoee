import {Configuration} from '../../types/Config';
import {
    ConfigurationRequiredWithNulls,
    ConfigurationRequiredWithNullsSchema,
} from '../../types/ConfigRequiredWithNulls';
import {undefToNull} from '../../utils/undefToNull';
import {validateSchema} from '../../utils/validate-schema';
import * as t from "io-ts";

export const validateConfigWithNulls = validateSchema(ConfigurationRequiredWithNullsSchema);

export const createConfigurationWithNulls = (
    x: Configuration,
): ConfigurationRequiredWithNulls => ({
    quiet: undefToNull(x.quiet),
    ignoreKeys: undefToNull(x.ignoreKeys),
    crawler: {rootPath: x.crawler?.rootPath ?? null, fileNameRegex: x.crawler?.fileNameRegex ?? null},
    verbose: undefToNull(x.verbose),
    fixIssues: undefToNull(x.fixIssues),
    ignoreLanguages: undefToNull(x.ignoreLanguages),
    ignoreRules: undefToNull(x.ignoreRules),
    files: undefToNull(x.files),
    disableUnicodeFix: undefToNull(x.disableUnicodeFix),
});
