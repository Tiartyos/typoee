import {Nullable} from '../../utils/utility-types';

export interface Error {
    errorMessage: Nullable<string>;
    matchedText: Nullable<string>;
}

interface IncompleteLocation {
    objectPath: string;
    ruleFunctionName: RuleFunctionName;
}

export interface Location extends IncompleteLocation {
    filePath: string;
}

interface RuleFunctionResultBase {
    location: Location;
}

export interface Fixed extends RuleFunctionResultBase{
    status: 'fixed';
    fixedValue: string;
    error: Error;
}

export interface Unfixable extends RuleFunctionResultBase{
    status: 'unfixable';
    error: Error;
}

export interface Okay extends RuleFunctionResultBase{
    status: 'okay';
}

export interface Ignored extends RuleFunctionResultBase{
    status: 'ignored';
}


export interface ErrorU extends RuleFunctionResultBase{
    status: 'error';
}

export type RuleFunction = (loc: Location, value: string) => RuleFunctionResult;
export type RuleFunctionResult = Fixed | Unfixable | Okay | Ignored | ErrorU;

export const CSuleFunctions = [
    'CS.abbreviation',
    'CS.conjunction',
    'CS.preposition',
    'CS.unit',
    'CS.hyphen',
    'CS.ellipsis',
    'CS.ordinalNumeral',
    'CS.quoteMark',
] as const;

export const ENruleFunctions = [
    'EN.abbreviation',
    'EN.conjunction',
    'EN.preposition',
    'EN.unit',
    'EN.hyphen',
    'EN.ellipsis',
    'EN.ordinalNumeral',
    'EN.quoteMark',
] as const;

// const RULE_FUNCTIONS = [...CSuleFunctions, ...ENruleFunctions] as const;

export type RuleFunctionName = typeof CSuleFunctions[number] | typeof ENruleFunctions[number];

export interface RuleDescriptor {
    name: RuleFunctionName;
    f: RuleFunction;
}
