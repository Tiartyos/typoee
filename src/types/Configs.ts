import {ConfigurationRequiredWithNulls} from '../../types/ConfigRequiredWithNulls';
import {YargsParams} from '../main/prepareYargsParams';

export interface Configs {
    defaultConfig: ConfigurationRequiredWithNulls;
    projectConfig: ConfigurationRequiredWithNulls;
    yargsParams: YargsParams;
}
