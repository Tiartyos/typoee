import {echo} from 'shelljs';
import {pipe} from 'ts-opt';
import {hideBin} from 'yargs/helpers';

import {ConfigurationRequiredWithNulls} from '../types/ConfigRequiredWithNulls';
import {getLanguageFilesPaths} from '../utils/getLanguageFilesPaths';
import {assembleConfig} from './main/assembleConfig';
import {prepareConfigsForAssemble} from './main/prepareConfigsForAssemble';
import {prepareYargsParams} from './main/prepareYargsParams';
import {processFileTranslationsDeepForAllLanguages} from './main/processLanguageFile';
import {CmdArgs, cmdArgsparser} from './main/yargsParser';

const yargsInp = cmdArgsparser.parseSync(hideBin(process.argv));
if (yargsInp.debugYargs){
    // eslint-disable-next-line no-console
    console.log('yargs:', JSON.stringify(yargsInp));
}
echo(JSON.stringify(process.argv));
const customProjectConfigLocation = './projectConfig.yaml';

export const loadFullConfig = (yargs: CmdArgs, userConfigPath: string): ConfigurationRequiredWithNulls =>pipe(
    yargs,
    prepareYargsParams,
    x => prepareConfigsForAssemble(x, userConfigPath),
    assembleConfig,
);

const fullConfig = loadFullConfig(yargsInp, customProjectConfigLocation);
// eslint-disable-next-line no-console
console.log('fullConfig', fullConfig);
// const config = loadDefaultConfig('./config.yaml');
// // eslint-disable-next-line no-console
// console.log(config);
const languageFilePaths = getLanguageFilesPaths(fullConfig);
const res = languageFilePaths.map(x => processFileTranslationsDeepForAllLanguages(fullConfig, x));
// eslint-disable-next-line no-console
console.log('res', res);

