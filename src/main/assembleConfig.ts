import {ConfigurationRequiredWithNulls} from '../../types/ConfigRequiredWithNulls';
import {CrawlerRequiredWithNulls} from '../../types/CrawlerRequiredWithNulls';
import {Configs} from '../types/Configs';
import {nullOrNext} from '../utils/nullOrNext';

export const assembleConfig = (configs: Configs): ConfigurationRequiredWithNulls => {
    const {projectConfig, defaultConfig: defC, yargsParams} = configs;

    const yargsParamCrawler: CrawlerRequiredWithNulls | null = yargsParams.filenameRegex && yargsParams.root ? {
        fileNameRegex: yargsParams.filenameRegex,
        rootPath: yargsParams.root,
    } : null;

    return {
        quiet: nullOrNext(yargsParams.quiet, projectConfig.quiet, defC.quiet),
        ignoreKeys: nullOrNext(null, projectConfig.ignoreKeys, defC.ignoreKeys),
        verbose: nullOrNext(yargsParams.verbose, projectConfig.verbose, defC.verbose),
        ignoreRules: nullOrNext(null, projectConfig.ignoreRules, defC.ignoreRules),
        ignoreLanguages: nullOrNext(null, projectConfig.ignoreLanguages, defC.ignoreLanguages),
        fixIssues: nullOrNext(yargsParams.fixIssues, projectConfig.fixIssues, defC.fixIssues),
        crawler: nullOrNext(yargsParamCrawler, projectConfig.crawler, defC.crawler),
        files: nullOrNext(yargsParams.file, projectConfig.files, defC.files),
        disableUnicodeFix: nullOrNext(null, projectConfig.disableUnicodeFix, defC.disableUnicodeFix),
    };
};
