import {load} from 'js-yaml';
import {cat, test as testsh} from 'shelljs';

import {ConfigurationSchema} from '../../types/Config';
import {ConfigurationRequiredWithNulls} from '../../types/ConfigRequiredWithNulls';
import {validateSchema} from '../../utils/validate-schema';
import {createConfigurationWithNulls, validateConfigWithNulls} from '../utils/config-helper';

const validateConfig = validateSchema(ConfigurationSchema);

export const loadConfig = (path:string, pathIsFromYargs: boolean): ConfigurationRequiredWithNulls => {
    const isPathValid = testsh('-f', path);
    if (!isPathValid && !pathIsFromYargs){
        return createConfigurationWithNulls({});
    }

    if (!isPathValid && pathIsFromYargs){
        // eslint-disable-next-line no-console
        console.error(`no configuration file found at: '${path}'`);
        throw new Error('configuration file missing');
    }

    const currentConfig = load(cat(path));

    const validatedPartialConfig = validateConfig(currentConfig)(
        'loadPartialConfig Failed, check config.yaml file for possible issues');

    const requiredConfigWithNulls = createConfigurationWithNulls(validatedPartialConfig);

    return validateConfigWithNulls(
        requiredConfigWithNulls)('loadDefaultConfig Failed, check config.yaml file for possible issues');
};
