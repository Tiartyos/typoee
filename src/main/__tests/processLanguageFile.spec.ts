import {expect} from 'chai';

import {ConfigurationRequiredWithNulls} from '../../../types/ConfigRequiredWithNulls';
import {Language} from '../../../types/Language';
import {processFileTranslationsDeepForAllLanguages} from '../processLanguageFile';

const mockConfig: ConfigurationRequiredWithNulls = {
    ignoreRules: {CS: {quoteMark: true}, EN: {}},
    ignoreLanguages: [
        'EN',
    ],
    ignoreKeys: {
        CS: ['CS.test.someNode.insideSomeNodeObj.uglyIgnoredKey'],
        EN: [],
    },
    quiet: false,
    fixIssues: true,
    crawler: {
        rootPath: 'src/main/__tests/files/output',
        fileNameRegex: 'uniqueName1',
    },
    files: [],
    verbose: false,
    disableUnicodeFix: false,
};


describe('processFileTranslationsDeep', () => {
    it.only('processFileTranslationsDeep', () => {
        const mockTranslationPath = 'src/main/__tests/files/output/uniqueName1.ts';
        expect(processFileTranslationsDeepForAllLanguages(mockConfig, mockTranslationPath)).to.eql('');
    });
});

