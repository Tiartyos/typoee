/* eslint-disable no-console  */

import * as chai from 'chai';

// eslint-disable-next-line @typescript-eslint/no-require-imports
const spy = require('chai-spies');

chai.use(spy);
const {expect} = chai;
chai.should();
const sandbox = chai.spy.sandbox();

const expectedRes1 = {
    crawler: {
        rootPath: 'test/data/src',
        fileNameRegex: 'translations',
    },
    filename: 'translations',
    fixIssues: true,
    quiet: false,
    ignoreLanguages: ['EN'],
    ignoreKeys: {
        CS: ['CS.test.someNode.insideSomeNodeObj.uglyIgnoredKey'],
        EN: [],
    },
    ignoreRules: {CS: {}, EN: {}},
    disableUnicodeFix: false,
};

// describe('loadDefaultConfig', () => {
//     it('loads config', () => {
//         expect(loadDefaultConfig('./src/main/__tests/mockconfig.yaml')).to.eql(expectedRes1);
//     });
//     it('missing required language prop, should throw error and console log', () => {
//         assert.throws(() => {
//             loadDefaultConfig('./src/main/__tests/mockconfig1.yaml');
//         }, Error);
//     });
//     it('missing required filename prop, should throw error and console log', () => {
//         assert.throws(() => {
//             loadDefaultConfig('./src/main/__tests/mockconfig2.yaml');
//         }, Error);
//     });
//     it('missing required rootpath prop, should throw error and console log', () => {
//         assert.throws(() => {
//             loadDefaultConfig('./src/main/__tests/mockconfig3.yaml');
//         }, Error);
//     });
//     it('invalid path, should throw error and console log', () => {
//         loadDefaultConfig('./src/main/__tests/mockconfig3.yaml');
//     });
// });

describe('loadConfig', () => {
    beforeEach(() => {
        sandbox.on(console, ['log', 'error']);
    });

    afterEach(() => {
        sandbox.restore();
    });

    // it('loads config', () => {
    //     expect(loadDefaultConfig('./src/main/__tests/mockconfig.yaml')).to.eql(expectedRes1);
    // });
    //
    // it('missing required language prop, should throw error and console log', () => {
    //     expect(() => loadDefaultConfig('./src/main/__tests/mockconfig123.yaml')).to.throw();
    //     expect(console.error).to.have.been.called.exactly(1);
    //     // eslint-disable-next-line max-len
    //     expect(console.error).to.have.been.called.with('no configuration file found at: \'./src/main/__tests/mockconfig123.yaml\'');
    // });
});
