/* eslint-disable max-lines */
export const testTranslations = {
    CS: {
        "test": {
            someItem: "some text k\u00A0CS.\u00A05.\u00A0lol",
            someNode: {
                "insideSomeNode": 1,
                "insideSomeNode2": 2,
                "insideSomeNodeObj": {
                    insideSomeNodeObj: "test",
                    uglyIgnoredKey: "aAbnMasda.5.5.fasfqwewq"
                },
            }
        },
    },
    EN: {
        "test": {
            someItem: "some text EN",
            someNode: {
                "insideSomeNode": 1,
                "insideSomeNode2": 2,
                "insideSomeNodeObj": {
                    insideSomeNodeObj: "test",
                    uglyIgnoredKey: "aAbnMasda.\u00A05.\u00A05.\u00A0fasfqwewq"
                },
            }
        },
    }
};
