// Responsibility:
// call every rule function for a specific language(except those excluded in Configuration file)
// pass result of each function to the next one as well as Status logs
// recursively call yourself with latest translation value until
// a passthrough hasn't ended without new RuleFunctionResultIncomplete "fixed" object
// before returning final Status log, remove duplicates

import {expect} from 'chai';
import {keys, reject} from 'lodash/fp';
import {opt} from 'ts-opt';

import {ConfigurationRequiredWithNulls} from '../../../types/ConfigRequiredWithNulls';
import {Languages} from '../../../types/Languages';
import {CS} from '../../ruleFunctions/CS';
import {
    RuleDescriptor,
    RuleFunctionResult,
} from '../../types/RuleFunctionsCommon';
import {TranslationContext} from '../processLanguageFile';
import {processTranslation} from '../processTranslation';

type Language = keyof Languages;
const mockConfigFile1: ConfigurationRequiredWithNulls = {
    crawler: {
        fileNameRegex: '',
        rootPath: '',
    },
    fixIssues: true,
    quiet: false,
    ignoreLanguages: ['EN'],
    ignoreKeys: {
        CS: ['CS.test.someNode.insideSomeNodeObj.uglyIgnoredKey'],
        EN: [],
    },
    ignoreRules: {
        CS: {},
        EN: {},
    },
    files: [],
    verbose: false,
    disableUnicodeFix: false,
};

const mockConfigFile2: ConfigurationRequiredWithNulls = {
    crawler: {
        fileNameRegex: '',
        rootPath: '',
    },
    fixIssues: true,
    quiet: false,
    ignoreLanguages: ['EN'],
    ignoreKeys: {
        CS: ['CS.test.someNode.insideSomeNodeObj.uglyIgnoredKey'],
        EN: [],
    },
    ignoreRules: {
        CS: {
            ordinalNumeral: true,
        },
        EN: {},
    },
    files: [],
    verbose: false,
    disableUnicodeFix: false,
};

const CSfunctionsWithoutOrdinalNumeral = CS.filter(x => x.name !== 'CS.ordinalNumeral');

const getFunctionsByLanguage = (lang: Language): ReadonlyArray<RuleDescriptor> => {
    switch (lang) {
        case 'CS': {
            return CS;
        }
        case 'EN': {
            return CS;
        }
        default:
            return CS;
    }
};

const getApplicableRuleFunctions = (
    language: Language,
    config: ConfigurationRequiredWithNulls,
): Array<RuleDescriptor> => {
    const ignoreRules = opt(config.ignoreRules).prop(language).orCrash(
        'ignoreRules are required field, respect config structure');
    const allRuleFunctions = getFunctionsByLanguage(language);
    const ignoreRulesForCurrentLanguage = keys(ignoreRules).map(x => `${language}.${x}`);

    return reject(x => ignoreRulesForCurrentLanguage.includes(x.name), allRuleFunctions);
};


describe('GetApplicableRuleFunctions', () => {
    it('returns array of all CS functions', () => {
        expect(getApplicableRuleFunctions('CS', mockConfigFile1)).to.eql(CS);
    });

    it('returns array of all CS functions except for ignored ones(ordinalNumeral)', () => {
        expect(getApplicableRuleFunctions('CS', mockConfigFile2)).to.eql(CSfunctionsWithoutOrdinalNumeral);
    });
});

export type RuleFunctionLog = Array<RuleFunctionResult>;

interface CurrentTranslationProps {
    fullRuleFunctionLog: RuleFunctionLog;
    ruleFunctions: Array<RuleDescriptor>;
    context: TranslationContext;
    passNumber: number;
    value: string;
    filePath: string;
    ignored: boolean;
}

const currentTranslationDefaultValues: CurrentTranslationProps = {
    fullRuleFunctionLog: [],
    ruleFunctions: [],
    context: {
        language: 'CS',
        config: mockConfigFile1,
        objPathTemp: 'CS',
        filePath: 'some/mock/filepath/.ts',
    },
    passNumber: -1,
    value: 'Some test.5.string k asd...',
    filePath: '',
    ignored: false,
};

const mockTranslationResult1 =
  [
      [
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.quoteMark',
              },
              status: 'okay',
          },
          {
              error: {
                  errorMessage: null,
                  matchedText: '5.',
              },
              fixedValue: 'Some test.5. string k asd...',
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.ordinalNumeral',
              },
              status: 'fixed',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.unit',
              },
              status: 'okay',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.hyphen',
              },
              status: 'okay',
          },
          {
              error: {
                  errorMessage: null,
                  matchedText: 'k',
              },
              fixedValue: 'Some test.5. string k asd...',
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.preposition',
              },
              status: 'fixed',
          },
          {
              error: {
                  errorMessage: null,
                  matchedText: '',
              },
              fixedValue: 'Some test.5. string k asd…',
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.ellipsis',
              },
              status: 'fixed',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.abbreviation',
              },
              status: 'okay',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.conjunction',
              },
              status: 'okay',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.quoteMark',
              },
              status: 'okay',
          },
          {
              error: {
                  errorMessage: null,
                  matchedText: '5.',
              },
              fixedValue: 'Some test. 5. string k asd…',
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.ordinalNumeral',
              },
              status: 'fixed',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.unit',
              },
              status: 'okay',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.hyphen',
              },
              status: 'okay',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.preposition',
              },
              status: 'okay',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.ellipsis',
              },
              status: 'okay',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.abbreviation',
              },
              status: 'okay',
          },
          {
              location: {
                  filePath: 'some/mock/filepath/.ts',
                  objectPath: 'CS',
                  ruleFunctionName: 'CS.conjunction',
              },
              status: 'okay',
          },
      ],
      'Some test. 5. string k asd…',
  ];


describe('processTranslation', () => {
    it('processTranslation', () => {
        expect(processTranslation(currentTranslationDefaultValues)).to.eql(mockTranslationResult1);
    });
});
