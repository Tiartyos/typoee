/* tslint:disable:no-unused-expression no-console */
import * as chai from 'chai';
import * as spies from 'chai-spies';
import {Node, SyntaxKind} from 'typescript';

import {assertSourceFileNode} from '../processLanguageFile';


chai.use(spies);
const {expect} = chai;
chai.should();
// const sandbox = chai.spy.sandbox();

describe('Assertions', () => {
    describe('assertSourceFileNode', () => {
        it('asserts Node obj kind to be SourceFile', () => {
            expect(() => assertSourceFileNode({kind: SyntaxKind.SourceFile} as Node)).to.not.throw();
            expect(() => assertSourceFileNode({kind: SyntaxKind.PropertyAssignment} as Node)).to.throw();
        });
    });
});
