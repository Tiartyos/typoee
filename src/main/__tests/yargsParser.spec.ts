import * as chai from 'chai';
import {jestSnapshotPlugin} from 'mocha-chai-jest-snapshot';
import {join, relative} from 'path';
import {exec, pwd} from 'shelljs';

chai.use(jestSnapshotPlugin());
const {expect} = chai;

const callExec = () => {
    const {stdout, stderr,code } = exec('node dist/src/Typoee.js -- -c ./ --debug-yargs');
    // const currentLocation = pwd().toString();
    // const yPath = 'D:\\webdesign\\moje\\typoee\\src\\main\\my_config\\my_config123456.ts'
    const currentLocation = 'D:\\webdesign\\moje\\typoee\\src\\main\\ruleFunctions';
    const yPath = '..\\..\\main\\my_config\\my_config123456.ts';

    const test = relative(currentLocation, yPath);
    const test1 = join(currentLocation, yPath);
    // console.log('Exit code:', code);
    // console.log('Program output:', stdout);
    // console.log('Program stderr:', stderr);
    console.log('')
};

it('foo', () => {
    callExec()
    expect({foo: 'bar'}).toMatchSnapshot();
});
