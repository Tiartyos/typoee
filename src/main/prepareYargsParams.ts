import {undefToNull} from '../../utils/undefToNull';
import {Nullable} from '../../utils/utility-types';

export interface YargsParams {
    config: Nullable<string>;
    file: Nullable<Array<string>>;
    filenameRegex: Nullable<string>;
    root: Nullable<string>;
    verbose: Nullable<boolean>;
    quiet: Nullable<boolean>;
    fixIssues: Nullable<boolean>;
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const prepareYargsParams = (yargsInp: any): YargsParams => ({
    config: undefToNull(yargsInp.c),
    file: undefToNull(yargsInp.f),
    filenameRegex: undefToNull(yargsInp.r),
    root: undefToNull(yargsInp.d),
    verbose: undefToNull(yargsInp.v),
    quiet: undefToNull(yargsInp.q),
    fixIssues: undefToNull(yargsInp.o),
});
