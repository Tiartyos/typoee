/* eslint-disable no-param-reassign */

// Responsibility:
// call every rule function for a specific language(except those excluded in Configuration file)
// pass result of each function to the next one as well as Status logs
// recursively call yourself with latest translation value until a passthrough
// hasn't ended without new RuleFunctionResultUncomplete "fixed" object
// before returning final Status log, remove duplicates


import {keys, reject, isEmpty, uniqBy} from 'lodash/fp';
import {opt, pipe} from 'ts-opt';

import {ConfigurationRequiredWithNulls} from '../../types/ConfigRequiredWithNulls';
import {Languages} from '../../types/Languages';
import {CS} from '../ruleFunctions/CS';
import {RuleDescriptor, RuleFunctionResult} from '../types/RuleFunctionsCommon';
import {TranslationContext} from './processLanguageFile';

type Language = keyof Languages;


const getFunctionsByLanguage = (lang: Language): ReadonlyArray<RuleDescriptor> => {
    switch (lang) {
        case 'CS': {
            return CS;
        }
        case 'EN': {
            return CS;
        }
        default:
            return CS;
    }
};

const getApplicableRuleFunctions = (
    language: Language,
    config: ConfigurationRequiredWithNulls,
): Array<RuleDescriptor> => {
    const ignoreRules = opt(config.ignoreRules).prop(language).orCrash(
        'ignoreRules are required field, respect config structure');
    const allRuleFunctions = getFunctionsByLanguage(language);
    const ignoreRulesForCurrentLanguage = keys(ignoreRules).map(x => language + x);

    return reject(x => ignoreRulesForCurrentLanguage.includes(x.name), allRuleFunctions);
};


export type RuleFunctionLog = Array<RuleFunctionResult>;

interface CurrentTranslationProps {
    fullRuleFunctionLog: RuleFunctionLog;
    ruleFunctions: Array<RuleDescriptor>;
    context: TranslationContext;
    passNumber: number;
    value: string;
    ignored: boolean;
}


type RuleFunctionAccumulator = readonly [accumulator: RuleFunctionLog, currentValue: string];
export type RuleFunctionLogAndValue = [fullLog: RuleFunctionLog, value: string];

const containtsFixedError = (results: RuleFunctionLog): boolean => results.filter(x => x.status === 'fixed').length > 0;

export const ruleFunctionLogErrsOnly = (fullRuleFunctionLog: RuleFunctionLog):RuleFunctionLog => pipe(
    fullRuleFunctionLog,
    x => x.filter(y => y.status === 'fixed'),
    y => uniqBy('error', y),
);

export const processTranslation = (
    {
        fullRuleFunctionLog,
        ruleFunctions,
        passNumber,
        value,
        context,
        ignored,
    }: CurrentTranslationProps,
): RuleFunctionLogAndValue => {
    const {config, language, filePath, objPathTemp} = context;
    ruleFunctions = isEmpty(ruleFunctions) ? getApplicableRuleFunctions(language, config) : ruleFunctions;
    passNumber++;

    if (ignored){
        const newfull: RuleFunctionLog = ruleFunctions.map(x =>({
            status: 'ignored',
            location: {
                ruleFunctionName: x.name,
                filePath,
                objectPath: objPathTemp,
            },
        }));

        return [newfull, value];
    }

    const currentRuleFunctionLogTemp = ruleFunctions.reduce(
        ([accumulator, val]: RuleFunctionAccumulator, currentRuleFunction) => {
            const newRuleFunctionResult = currentRuleFunction.f({
                objectPath: objPathTemp,
                ruleFunctionName: currentRuleFunction.name,
                filePath,
            }, val);
            const newAccumulator = accumulator.concat(newRuleFunctionResult);
            const newValue = newRuleFunctionResult.status === 'fixed' ? newRuleFunctionResult.fixedValue : val;

            return [newAccumulator, newValue] as const;
        },
        [[], value],
    );
    const [currentRuleFunctionLogTempRes, newValue] = currentRuleFunctionLogTemp;

    if (containtsFixedError(currentRuleFunctionLogTempRes)) {
        const newfull = fullRuleFunctionLog.concat(currentRuleFunctionLogTempRes);

        return processTranslation(
            {
                passNumber,
                fullRuleFunctionLog: newfull,
                ruleFunctions,
                value: newValue,
                context: {
                    config,
                    language,
                    objPathTemp,
                    filePath,
                },
                ignored,
            },
        );
    } else {
        return [fullRuleFunctionLog, value];
    }
};

