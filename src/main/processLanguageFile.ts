import {flatMap, head} from 'lodash/fp';
import * as path from 'path';
import {cat, ShellString} from 'shelljs';
import {id, isString, opt, pipe} from 'ts-opt';
import {
    createPrinter,
    createSourceFile,
    EmitHint,
    Identifier,
    Node,
    NumericLiteral,
    ObjectLiteralElementLike,
    ObjectLiteralExpression,
    PropertyAssignment,
    PropertyName,
    ScriptTarget,
    SourceFile,
    StringLiteral,
    SyntaxKind,
    VariableStatement,
} from 'typescript';

import {ConfigurationRequiredWithNulls} from '../../types/ConfigRequiredWithNulls';
import {Language, LanguageSchema} from '../../types/Language';
import {validateSchema} from '../../utils/validate-schema';
import {processTranslation, RuleFunctionLog} from './processTranslation';
import {nonBreakableSpace} from "../../constants/constants";

const getNameFromPath = (translationPath: string): string => {
    const regEx = new RegExp(/(?<=[\p{L}|.|/]*)([\p{L}]*.[.]{1}ts)/gu);
    const matched = opt(translationPath.match(regEx)).orCrash('getNameFromPath - Invalid index');

    return matched[0];
};

export const makeSourceFile = (_config: ConfigurationRequiredWithNulls, translationPath: string): SourceFile => {
    const filename = getNameFromPath(translationPath);
    const data = cat(translationPath);

    return createSourceFile(
        filename, data, ScriptTarget.Latest,
    );
};

export const assertVariableStatementNode: (x: Node) => asserts x is VariableStatement = x => {
    if (x.kind !== SyntaxKind.VariableStatement) {
        throw new Error(`expected VariableStatement kind, received: ${x.kind}`);
    }
};

export const assertSourceFileNode: (x: Node) => asserts x is SourceFile = x => {
    if (x.kind !== SyntaxKind.SourceFile) {
        throw new Error(`expected SourceFile kind, received: ${x.kind}`);
    }
};

export const assertObjectLiteralExpression: (x: Node) => asserts x is ObjectLiteralExpression = x => {
    if (x.kind !== SyntaxKind.ObjectLiteralExpression) {
        throw new Error(`expected ObjectLiteralExpression kind, received: ${x.kind}`);
    }
};

export const assertPropertyAssignment: (x: Node) => asserts x is PropertyAssignment = x => {
    if (x.kind !== SyntaxKind.PropertyAssignment) {
        throw new Error(`expected PropertyAssignment kind, received: ${x.kind}`);
    }
};
export const assertIdentifier: (x: PropertyName | undefined) => asserts x is Identifier = x => {
    if (x?.kind !== SyntaxKind.Identifier) {
        throw new Error(`expected Identifier kind, received: ${JSON.stringify(x)}`);
    }
};

export const assertStringLiteral: (x: Node) => asserts x is StringLiteral = x => {
    if (x.kind !== SyntaxKind.StringLiteral) {
        throw new Error(`expected StringLiteral kind, received: ${JSON.stringify(x)}`);
    }
};

export const assertNumericLiteral: (x: Node) => asserts x is NumericLiteral = x => {
    if (x.kind !== SyntaxKind.NumericLiteral) {
        throw new Error(`expected NumericLiteral kind, received: ${JSON.stringify(x)}`);
    }
};

export const getName = (x: PropertyAssignment): string => {
    const {name} = x;

    switch (name.kind) {
        case SyntaxKind.Identifier: {
            assertIdentifier(name);

            return name.text;
        }
        case SyntaxKind.StringLiteral: {
            assertPropertyAssignment(x);
            assertStringLiteral(name);

            return name.text;
        }
        case SyntaxKind.NumericLiteral: {
            assertPropertyAssignment(x);
            assertNumericLiteral(name);

            return name.text;
        }
        default:
            return '';
    }
};

const processObjectLiteralProperty = (x: ObjectLiteralElementLike): [string, ObjectLiteralExpression] => {
    assertPropertyAssignment(x);
    const {initializer} = x;
    assertObjectLiteralExpression(initializer);

    const test = getName(x);
    // eslint-disable-next-line no-console
    console.log(test);

    return [getName(x), initializer];
};

const processStringLiteral = (x: Node): string => {
    assertStringLiteral(x);
    const {text} = x;

    return text;
};

export interface TranslationContext {
    config: ConfigurationRequiredWithNulls;
    language: Language;
    objPathTemp: string;
    filePath: string;
}

const getShouldBeIgnored = (
    language: Language,
    config: ConfigurationRequiredWithNulls,
    objPath: string,
): boolean => {
    const ignoredObjPaths = opt(config.ignoreKeys).prop(language).orCrash(
        'ignoreKeys are required field, respect config structure');

    return ignoredObjPaths.includes(objPath);
};

export const checkInitializerKind = (
    x: ObjectLiteralElementLike,
    context: TranslationContext,
): RuleFunctionLog => {
    assertPropertyAssignment(x);

    const initializer = opt(x).prop('initializer').orCrash('missing initializer');
    const name = getName(x);

    if (initializer.kind === SyntaxKind.StringLiteral) {
        const text = processStringLiteral(initializer);
        const {config, language, filePath, objPathTemp} = context;

        const newObjPath = `${objPathTemp}.${name}`;

        const newContext: TranslationContext = {
            config,
            objPathTemp: newObjPath,
            language,
            filePath,
        };

        const ignored = getShouldBeIgnored(language, config, newObjPath);

        const allRulesApplied = processTranslation(
            {
                passNumber: -1,
                value: text,
                fullRuleFunctionLog: [],
                ruleFunctions: [],
                context: newContext,
                ignored,
            },
        );
        const [fullLog] = allRulesApplied;

        if (config.fixIssues) {
            assertStringLiteral(initializer);
            initializer.text = opt(fullLog.filter(y => y.status === 'fixed')).last()
            .prop('fixedValue').narrow(isString).orElse(text);
        }

        return fullLog;
    } else if (initializer.kind === SyntaxKind.ObjectLiteralExpression) {
        assertObjectLiteralExpression(initializer);
        processObjectLiteralProperty(x);
        const {config, language, filePath, objPathTemp} = context;

        const newObjPath = `${objPathTemp}.${name}`;

        const newContext: TranslationContext = {
            config,
            objPathTemp: newObjPath,
            language,
            filePath,
        };

        return flatMap((y => checkInitializerKind(y, newContext)), initializer.properties);
    } else {
        return [];
    }
};

export const processLanguageFile = (
    config: ConfigurationRequiredWithNulls,
    translationPath: string,
): [SourceFile, Array<[string, ObjectLiteralExpression]>] => {
    const sourceFile = makeSourceFile(config, translationPath);
    assertSourceFileNode(sourceFile);
    const [variableStatementNode] = sourceFile.statements;
    assertVariableStatementNode(variableStatementNode);
    const translations = opt(variableStatementNode.declarationList.declarations).chainToOpt(head).prop('initializer')
    .orCrash('not a prop');
    assertObjectLiteralExpression(translations);

    return [sourceFile, translations.properties.map(processObjectLiteralProperty)];
};

const outputlogs = (
    config: ConfigurationRequiredWithNulls,
    logs: RuleFunctionLog,
) => {
    if (config.verbose) {
        // eslint-disable-next-line no-console
        console.log(logs);
    } else if (config.quiet) {
        // eslint-disable-next-line no-console
        console.log(
            logs.filter(x => x.status === 'error'),
        );
    } else {
        // eslint-disable-next-line no-console
        console.log(
            logs.filter(x => x.status !== 'okay'),
        );
    }
};

export const processFileTranslationsDeepForAllLanguages = (
    config: ConfigurationRequiredWithNulls,
    translationPath: string,
): void => {
    const languageNodes = processLanguageFile(config, translationPath);
    const [sourceFile, languagePairs] = languageNodes;

    // If 'fixIssues': true in config, the languageNode is mutated in place

    const fullRuleFunctionLogs = flatMap(
        x => {
            const [languageU, languageNode] = x;

            const language = validateSchema(LanguageSchema)(languageU)('Invalid language');

            return flatMap(y => checkInitializerKind(
                y,
                {
                    config,
                    language,
                    filePath: translationPath,
                    objPathTemp: language,
                },
            ), languageNode.properties);
        }
        , languagePairs);


    // console.log(fullRuleFunctionLogs);
    outputlogs(config, fullRuleFunctionLogs);

    if (config.fixIssues) {
        const printer = createPrinter();
        const fixedUnicode = (x: string):string => unescape(x.replace(/\\u/g, '%u'));
        const res = pipe(
            printer.printNode(EmitHint.Unspecified, sourceFile, sourceFile),
            config.disableUnicodeFix ? id : fixedUnicode,
        );
        // console.log('mutatedFile>>>', mutatedFileStr, '<<<mutatedFile');
        // eslint-disable-next-line no-console
        console.log('output path', translationPath);

        new ShellString(res).to(translationPath);
    }
};
