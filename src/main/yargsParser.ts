import yargs from 'yargs';

export const cmdArgsparser = yargs([])
    .scriptName('typoee')
    .usage('Usage: $0 -f translations.ts -o false -q true')
    .example(
        '$0 -w 5 -h 6',
        'Returns the area (30) by multiplying the width with the height.',
    )
    .boolean('overwrite')
    .option('o', {
        alias: 'overwrite',
        describe: 'Overwrite found typographical issues with correct ones.',
        type: 'boolean',
    })
    .boolean('quiet')
    .option('q', {
        alias: 'quiet',
        describe: 'Quiet mode only outputs error logs. If false, the fixed translation logs are included as well.',
        type: 'boolean',
    })
    .boolean('verbose')
    .option('v', {
        alias: 'verbose',
        describe: 'Verbose mode outputs error, fixed, okay, unfixable status logs.',
        type: 'boolean',
    })
    .option('d', {
        alias: 'dir',
        describe: 'Directory for the crawler. FileNameRegex must be defined as well for this to have any effect.',
        type: 'string',
        nargs: 1,
    })
    .option('r', {
        alias: 'filename-regex',
        describe: 'FileNameRegex for the crawler. Directory must be defined for this to have any effect.',
        type: 'string',
        nargs: 1,
    })
    .option('f', {
        alias: 'file',
        describe: 'Path to file or files to be processed.',
        type: 'string',
    })
    .array('f')
    .option('c', {
        alias: 'config',
        describe: 'Path to config.',
        type: 'string',
    })
    .describe('help', 'Show help.') // Override --help usage message.
    .describe('version', 'Show version number.') // Override --version usage message.
    .epilog('copyright 2022')
    .help()
    .alias('h', 'help')
    .version()
    .alias('w', 'version')
    .option('debug-yargs', {
        alias: 'debug-yargs',
        describe: 'Debug mode to display yargs values.',
        type: 'boolean',
    })
    .strict(true)
;

export type CmdArgs = ReturnType<(typeof cmdArgsparser)['parseSync']>;
