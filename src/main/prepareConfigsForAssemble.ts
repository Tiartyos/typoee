import {defaultConfig} from '../../constants/configs';
import {defaultProjectConfigLocation} from '../../constants/constants';
import {Nullable} from '../../utils/utility-types';
import {Configs} from '../types/Configs';
import {loadConfig} from './loadConfig';
import {YargsParams} from './prepareYargsParams';


export const prepareConfigsForAssemble = (yargsParamsInp: YargsParams, projectConfigPath: Nullable<string>): Configs =>{
    const projectConfig = loadConfig(projectConfigPath ?? defaultProjectConfigLocation, false);

    return {
        defaultConfig,
        projectConfig,
        yargsParams: yargsParamsInp,
    };
};
