import {compact} from 'lodash/fp';
import {head, opt} from 'ts-opt';

import {nonBreakableSpace} from '../../../constants/constants';
import {replaceCharAtMatch} from '../../../utils/replaceCharAtMatch';
import {RuleFunction} from '../../types/RuleFunctionsCommon';


export const CSordinalNumeral: RuleFunction =
  (location, value) => {
      const regex1 = /(?<=[\p{L}]*[ | |.]{1})\d+(\.){1}(?=[\p{L}])/u;
      const regex2 = /(?<=(\.){1})\d+(\.){1}(?=[\p{L}]*[ | |.]{1})/u;

      const matched1 = opt(value.match(regex1)).orNull();
      const matched2 = opt(value.match(regex2)).orNull();
      const firstMatched = head(compact([matched2, matched1])).orNull();

      return replaceCharAtMatch(
          {
              replacementValue: [nonBreakableSpace],
              replacePosition: matched1 ? 'end' : 'beginning',
              location,
              firstMatched,
              value,
              numberOfCharactersToReplace: 0,
          },
      );
  };
