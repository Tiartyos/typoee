import {opt} from 'ts-opt';

import {nonBreakableSpace} from '../../../constants/constants';
import {replaceCharAtMatch} from '../../../utils/replaceCharAtMatch';
import {RuleFunction} from '../../types/RuleFunctionsCommon';


export const CSpreposition: RuleFunction =
  (location, value) => {
      const regex = /(?<=(^| ))([KOSUVZkosuvz]|do|ke|na|od|po|se|ve|za|ze)(?= )/;
      const firstMatched = opt(value.match(regex)).orNull();

      return replaceCharAtMatch(
          {
              replacementValue: [nonBreakableSpace],
              replacePosition: 'end',
              location,
              firstMatched,
              value,
              numberOfCharactersToReplace: 1,
          },
      );
  };
