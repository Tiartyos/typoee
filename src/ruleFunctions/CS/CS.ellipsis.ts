import {opt} from 'ts-opt';

import {ellipsisDotDotDot, ellipsisVypustka} from '../../../constants/constants';
import {replaceCharAtMatch} from '../../../utils/replaceCharAtMatch';
import {RuleFunction} from '../../types/RuleFunctionsCommon';


export const CSellipsis: RuleFunction =
  (location, value) => {
      const regex = /(?<=\w(\.{3})+)/;
      const firstMatched = opt(value.match(regex)).orNull();

      return replaceCharAtMatch(
          {
              replacementValue: [ellipsisVypustka],
              replacePosition: 'everywhere',
              valueToReplace: ellipsisDotDotDot,
              location,
              firstMatched,
              value,
              numberOfCharactersToReplace: 1,

          },
      );
  };
