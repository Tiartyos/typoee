import {opt} from 'ts-opt';

import {quotation, quotationBeginning, quotationEnd} from '../../../constants/constants';
import {replaceCharAtMatch} from '../../../utils/replaceCharAtMatch';
import {RuleFunction} from '../../types/RuleFunctionsCommon';


export const CSquoteMark: RuleFunction =
  (location, value) => {
      const regex = /(?<= ")([^.,])*(?=")/;
      const firstMatched = opt(value.match(regex)).orNull();

      return replaceCharAtMatch(
          {
              replacementValue: [quotationBeginning, quotationEnd],
              replacePosition: 'beginning-and-end',
              valueToReplace: quotation,
              location,
              firstMatched,
              value,
              numberOfCharactersToReplace: 1,
          },
      );
  };
