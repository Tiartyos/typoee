import {RuleDescriptor} from '../../types/RuleFunctionsCommon';
import {CSabbreviation} from './CS.abbreviation';
import {CSconjunction} from './CS.conjunction';
import {CSellipsis} from './CS.ellipsis';
import {CShyphen} from './CS.hyphen';
import {CSpreposition} from './CS.preposition';
import {CSquoteMark} from './CS.quoteMark';
import {CSordinalNumeral} from './CSordinalNumeral';
import {CSunit} from './CSunit';

export const CS: ReadonlyArray<RuleDescriptor> = [
    {
        name: 'CS.quoteMark',
        f: CSquoteMark,
    },
    {
        name: 'CS.ordinalNumeral',
        f: CSordinalNumeral,
    },
    {
        name: 'CS.unit',
        f: CSunit,
    },
    {
        name: 'CS.hyphen',
        f: CShyphen,
    },
    {
        name: 'CS.preposition',
        f: CSpreposition,
    },
    {
        name: 'CS.ellipsis',
        f: CSellipsis,
    },
    {
        name: 'CS.abbreviation',
        f: CSabbreviation,
    },
    {
        name: 'CS.conjunction',
        f: CSconjunction,
    },
];
