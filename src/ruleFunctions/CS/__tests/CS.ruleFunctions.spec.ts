/* tslint:disable:no-unused-expression no-console */
/* eslint-disable max-len */
import * as chai from 'chai';
import * as spies from 'chai-spies';

import {Location} from '../../../types/RuleFunctionsCommon';
import {CSabbreviation} from '../CS.abbreviation';
import {CSconjunction} from '../CS.conjunction';
import {CSellipsis} from '../CS.ellipsis';
import {CShyphen} from '../CS.hyphen';
import {CSpreposition} from '../CS.preposition';
import {CSquoteMark} from '../CS.quoteMark';
import {CSordinalNumeral} from '../CSordinalNumeral';
import {CSunit} from '../CSunit';

chai.use(spies);
const {expect} = chai;
chai.should();
// const sandbox = chai.spy.sandbox();

const csText1 = 'Soubory cookies používáme k vylepšení obsahu webu, jeho personalizaci a analýze používání webu uživateli.';
const csText2 = 'Soubory cookies používáme k vylepšení obsahu webu, jeho personalizaci a analýze používání webu uživateli.';
const csText3 = 'Soubory cookies používáme k vylepšení obsahu webu, jeho personalizaci a analýze používání webu uživateli.';
const csText4 = 'Veškerá data, tj. informace poskytnuté Poskytovatelem Objednateli se jejich poskytnutím stávají vlastnictvím Objednatele.';
const csText5 = 'Veškerá data, tj. informace poskytnuté Poskytovatelem Objednateli se jejich poskytnutím stávají vlastnictvím Objednatele.';
const csText6 = '1. Průměrná realizační cena (Kč/1 kg živé hmotnosti, plemenných jalovic za kus)';
const csText7 = '1. Průměrná realizační cena (Kč/1 kg živé hmotnosti, plemenných jalovic za kus)';
const csText8 = 'Načtení "Subjekt - chovatel"';
const csText9 = 'Načtení "Subjekt ‐ chovatel"';
const csText10 = 'Načítám...';
const csText11 = 'Načítám…';
const csText12 = 'Uložení "Provozně-ekonomické ukazatele"';
const csText13 = 'Uložení „Provozně-ekonomické ukazatele“';
const csText15 = 'Dat.1. zap.';
const csText16 = 'Dat. 1.zap.';
const csText17 = 'Dat. 1. zap.';

const genericPaths: Omit<Location, 'ruleFunctionName'> = {
    objectPath: 'test.some.item',
    filePath: './mockfilePath.ts',
};

interface Paths {
    objectPath: Location['objectPath'];
    filePath: Location['filePath'];
}

const createMockLocation = (paths: Paths) => (ruleFunctionName: Location['ruleFunctionName']): Location => ({
    ruleFunctionName, filePath: paths.filePath, objectPath: paths.objectPath,
});

describe('CS.ruleFunctions', () => {
    describe('CS.preposition', () => {
        it('returns okay for correct string', () => {
            expect(CSpreposition(createMockLocation(genericPaths)('CS.preposition'), csText2)).to.eql(
                {
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.preposition',
                    },
                    status: 'okay',
                },
            );

            it('fix missing non-breaking space at preposition', () => {
                expect(CSpreposition(createMockLocation(genericPaths)('CS.preposition'), csText1)).to.eql(
                    {
                        location: {
                            filePath: './mockfilePath.ts',
                            objectPath: 'test.some.item',
                            ruleFunctionName: 'CS.preposition',
                        },
                        status: 'okay',
                    },
                );
            });
        });
    });

    describe('CS.conjunction', () => {
        it('returns okay for correct string', () => {
            expect(CSconjunction(createMockLocation(genericPaths)('CS.conjunction'), csText3)).to.eql(
                {
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.conjunction',
                    },
                    status: 'okay',
                },
            );
        });

        it('fix missing non-breaking space at conjunction', () => {
            expect(CSconjunction(createMockLocation(genericPaths)('CS.conjunction'), csText1)).to.eql(
                {
                    error: {
                        errorMessage: null,
                        matchedText: 'a',
                    },
                    fixedValue: 'Soubory cookies používáme k vylepšení obsahu webu, jeho personalizaci a analýze používání webu uživateli.',
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.conjunction',
                    },
                    status: 'fixed',
                },
            );
        });
    });

    describe('CS.abbreviation', () => {
        it('returns okay for correct string', () => {
            expect(CSabbreviation(createMockLocation(genericPaths)('CS.abbreviation'), csText5)).to.eql(
                {
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.abbreviation',
                    },
                    status: 'okay',
                },
            );
        });

        it('fix missing non-breaking space at abbreviation', () => {
            expect(CSabbreviation(createMockLocation(genericPaths)('CS.abbreviation'), csText4)).to.eql(
                {
                    error: {
                        errorMessage: null,
                        matchedText: 'tj.',
                    },
                    fixedValue: csText5,
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.abbreviation',
                    },
                    status: 'fixed',
                },
            );
        });
    });

    describe('CS.units', () => {
        it('returns okay for correct string', () => {
            expect(CSunit(createMockLocation(genericPaths)('CS.unit'), csText7)).to.eql(
                {
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.unit',
                    },
                    status: 'okay',
                },
            );
        });

        it('fix missing non-breaking space at unit', () => {
            expect(CSunit(createMockLocation(genericPaths)('CS.unit'), csText6)).to.eql(
                {
                    error: {
                        errorMessage: null,
                        matchedText: ' kg',
                    },
                    fixedValue: csText7,
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.unit',
                    },
                    status: 'fixed',
                },

            );
        });
    });

    describe('CS.hyphen', () => {
        it('returns okay for correct string', () => {
            expect(CShyphen(createMockLocation(genericPaths)('CS.hyphen'), csText9)).to.eql(
                {
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.hyphen',
                    },
                    status: 'okay',
                },
            );
        });

        it('fix missing non-breaking space at hyphen(Spojovník)', () => {
            expect(CShyphen(createMockLocation(genericPaths)('CS.hyphen'), csText8)).to.eql(
                {
                    error: {
                        errorMessage: null,
                        matchedText: '-',
                    },
                    fixedValue: csText9,
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.hyphen',
                    },
                    status: 'fixed',
                },

            );
        });
    });

    describe('CS.ellipsis', () => {
        it('returns okay for correct string', () => {
            expect(CSellipsis(createMockLocation(genericPaths)('CS.ellipsis'), csText11)).to.eql(
                {
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.ellipsis',
                    },
                    status: 'okay',
                },
            );
        });

        it('fix misuse of dot dot dot ellipsis instead of výpustka: …', () => {
            expect(CSellipsis(createMockLocation(genericPaths)('CS.ellipsis'), csText10)).to.eql(
                {
                    error: {
                        errorMessage: null,
                        matchedText: '',
                    },
                    fixedValue: 'Načítám…',
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.ellipsis',
                    },
                    status: 'fixed',
                },
            );
        });
    });


    describe('CS.quoteMark', () => {
        it('returns okay for correct string', () => {
            expect(CSquoteMark(createMockLocation(genericPaths)('CS.quoteMark'), csText13)).to.eql(
                {
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.quoteMark',
                    },
                    status: 'okay',
                },
            );
        });

        it('fix misuse of quoteMark', () => {
            expect(CSquoteMark(createMockLocation(genericPaths)('CS.quoteMark'), csText12)).to.eql(
                {
                    error: {
                        errorMessage: null,
                        matchedText: 'Provozně-ekonomické ukazatele',
                    },
                    fixedValue: 'Uložení „Provozně-ekonomické ukazatele“',
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.quoteMark',
                    },
                    status: 'fixed',
                },
            );
        });
    });

    describe('CS.ordinalNumber', () => {
        it('returns okay for correct string', () => {
            expect(CSordinalNumeral(createMockLocation(genericPaths)('CS.ordinalNumeral'), csText17)).to.eql(
                {
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.ordinalNumeral',
                    },
                    status: 'okay',
                },

            );
        });

        it('fix missing space after numeral', () => {
            expect(CSordinalNumeral(createMockLocation(genericPaths)('CS.ordinalNumeral'), csText15)).to.eql(
                {
                    error: {
                        errorMessage: null,
                        matchedText: '1.',
                    },
                    fixedValue: csText17,
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.ordinalNumeral',
                    },
                    status: 'fixed',
                },

            );
        });

        it('fix missing space after numeral', () => {
            expect(CSordinalNumeral(createMockLocation(genericPaths)('CS.ordinalNumeral'), csText16)).to.eql(
                {
                    error: {
                        errorMessage: null,
                        matchedText: '1.',
                    },
                    fixedValue: csText17,
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.ordinalNumeral',
                    },
                    status: 'fixed',
                },

            );
        });

        it('fix missing space before numeral', () => {
            expect(CSordinalNumeral(createMockLocation(genericPaths)('CS.ordinalNumeral'), csText16)).to.eql(
                {
                    error: {
                        errorMessage: null,
                        matchedText: '1.',
                    },
                    fixedValue: 'Dat. 1. zap.',
                    location: {
                        filePath: './mockfilePath.ts',
                        objectPath: 'test.some.item',
                        ruleFunctionName: 'CS.ordinalNumeral',
                    },
                    status: 'fixed',
                },
            );
        });
    });
});
