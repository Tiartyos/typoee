import {opt} from 'ts-opt';

import {nonBreakableSpace} from '../../../constants/constants';
import {replaceCharAtMatch} from '../../../utils/replaceCharAtMatch';
import {RuleFunction} from '../../types/RuleFunctionsCommon';


export const CSunit: RuleFunction =
  (location, value) => {
      const regex = /(?<=\d) (hod|mg|kg|h|l|ml|°C|Kč|\p{Sc})(?=(\b| ))/u;
      const firstMatched = opt(value.match(regex)).orNull();

      return replaceCharAtMatch(
          {
              replacementValue: [nonBreakableSpace],
              replacePosition: 'beginning',
              location,
              firstMatched,
              value,
              numberOfCharactersToReplace: 1,
          },
      );
  };
