import {opt} from 'ts-opt';

import {hyphenPomlcka, hyphenSpojovnik} from '../../../constants/constants';
import {replaceCharAtMatch} from '../../../utils/replaceCharAtMatch';
import {RuleFunction} from '../../types/RuleFunctionsCommon';


export const CShyphen: RuleFunction =
  (location, value) => {
      const regex = /(?<=[\p{L}]( | )+)-(?=( | )+[\p{L}])/u;
      const firstMatched = opt(value.match(regex)).orNull();

      return replaceCharAtMatch(
          {
              replacementValue: [hyphenPomlcka],
              replacePosition: 'everywhere',
              valueToReplace: hyphenSpojovnik,
              location,
              firstMatched,
              value,
              numberOfCharactersToReplace: 1,
          },
      );
  };
