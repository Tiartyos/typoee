import {identity, isString, join} from 'lodash/fp';
import {take} from './take';
import {drop} from './drop';

interface SplitAt1Fn {
    <T>(xs: Array<T>): [Array<T>, Array<T>];

    (xs: string): [string, string];
}

/**
 * Split array/string after {@param n} first items into two parts.
 *
 * @example
 * ```ts
 * splitAt(1)([0, 1, 2]) // [[0], [1, 2]]
 * splitAt(2)('abc') // ['ab', 'c']
 * ```
 *
 * @param n
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const splitAt: (n: number) => SplitAt1Fn = n => (xs: any): any => [
    take(n)(xs),
    drop(n)(xs),
].map(isString(xs) ? join('') : identity);
