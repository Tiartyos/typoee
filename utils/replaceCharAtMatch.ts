import {pipe} from 'fp-ts/function';
import {opt} from 'ts-opt';

import {RuleFunctionResult, Location} from '../src/types/RuleFunctionsCommon';
import {replaceFromTo} from './replaceFromTo';

export const REPLACEMENT_POSITION = ['beginning', 'end', 'everywhere', 'beginning-and-end'] as const;
export type ReplacePosition = typeof REPLACEMENT_POSITION[number];

export interface ReplacementOptions {
    value: string;
    firstMatched: RegExpMatchArray | null;
    valueToReplace?: string;
    numberOfCharactersToReplace: number;
    replacementValue: Array<string>;
    replacePosition: ReplacePosition;
    location: Location;
}

export type ReplaceCharAtMatchFunction = (input: ReplacementOptions) => RuleFunctionResult;
export type ReplaceByConf = (
    input: Omit<ReplacementOptions, 'ruleFunctionName' | 'path' | 'firstMatched'> & {
        firstMatched: RegExpMatchArray,
    }) => string;

const replaceByConf: ReplaceByConf = ({
    valueToReplace,
    replacementValue,
    replacePosition,
    value,
    firstMatched,
    numberOfCharactersToReplace,
}) => {
    // Default case is REPLACEMENT_POSITION: 'end'
    const firstMatchedIndex = opt(firstMatched.index).orCrash('Invalid index');
    const endOfMatchIdx = firstMatchedIndex + firstMatched[0].length;

    switch (replacePosition) {
        case 'beginning': {
            return replaceFromTo(
                firstMatchedIndex, firstMatchedIndex + numberOfCharactersToReplace, replacementValue[0])(value);
        }
        case 'beginning-and-end': {
            return pipe(replaceFromTo(
                firstMatchedIndex - numberOfCharactersToReplace, firstMatchedIndex, replacementValue[0])(value),
            x => replaceFromTo(
                endOfMatchIdx, endOfMatchIdx + numberOfCharactersToReplace, replacementValue[1])(x),
            );
        }
        case 'everywhere': {
            return value.replace(opt(valueToReplace).orCrash('Incorrect valueToReplace'), replacementValue[0]);
        }
        default:
            return replaceFromTo(
                endOfMatchIdx, endOfMatchIdx + numberOfCharactersToReplace, replacementValue[0])(value);
    }
};

export const replaceCharAtMatch: ReplaceCharAtMatchFunction = ({
    replacementValue,
    replacePosition,
    valueToReplace,
    firstMatched,
    value,
    location, numberOfCharactersToReplace,
}): RuleFunctionResult => (
    opt(firstMatched).mapFlow(
        x => [replaceByConf({
            valueToReplace,
            replacementValue,
            replacePosition,
            value,
            firstMatched: x,
            numberOfCharactersToReplace,
            location,
        }), x] as const,
        ([y, x]): RuleFunctionResult => ({
            status: 'fixed',
            fixedValue: y,
            error: {
                errorMessage: null,
                matchedText: x[0],
            },
            location,
        }),
    ).orElse({
        status: 'okay',
        location,
    }));
