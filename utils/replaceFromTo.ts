/* tslint:disable:no-unused-expression no-console */
import {pipe} from 'ts-opt';

import {insertElemsAt} from './insert-elems-at';
import {removeManyAt} from './remove-many-at';


type ReplaceFromTo = (
    fromIdx: number,
    toIdx: number,
    newValue: string,
) => (source: string) => string;

export const replaceFromTo: ReplaceFromTo = (fromIdx, toIdx, newValue) => (source): string => {
    const reversedOrder = fromIdx > toIdx;
    const fromIdxC = reversedOrder ? toIdx : fromIdx;
    const toIdxC = reversedOrder ? fromIdx : toIdx;
    const charsToRemove = toIdxC > fromIdxC ? toIdxC - fromIdxC : toIdxC - fromIdxC;

    // console.log(fromIdxC, charsToRemove, source);
    // console.log(removeManyAt(fromIdxC)(charsToRemove)(source));


    return pipe(
        removeManyAt(fromIdxC)(charsToRemove)(source),
        x => insertElemsAt(fromIdxC)(x)(newValue),
    );
};
