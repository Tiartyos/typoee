import {uniqBy} from 'lodash/fp';
import {normalize} from 'path';
import {find, pwd, test} from 'shelljs';
import {opt} from 'ts-opt';

import {ConfigurationRequiredWithNulls} from '../types/ConfigRequiredWithNulls';


export const getLanguageFilesPaths = (configuration: ConfigurationRequiredWithNulls): Array<string> => {
    const crawlerPath = configuration.crawler?.rootPath;
    const crawlerRegex = configuration.crawler?.fileNameRegex;
    const filesFromConfig = configuration.files;

    if (crawlerPath && crawlerRegex){
        const regex = new RegExp(crawlerRegex, 'u');

        console.log('find(crawlerPath)', crawlerPath, find(crawlerPath));
        const languageFilesPaths = find(crawlerPath).grep(regex).trim().split('\n').filter(
            x => x).filter(x=>test('-f', x));
        console.log('languageFilesPaths', languageFilesPaths);

        return uniqBy(x => normalize(x), languageFilesPaths.concat(filesFromConfig ?? []));
    }

    return opt(filesFromConfig).orCrash('No valid translation paths have been supplied.');
};
