import {Nullable} from "./utility-types";

export const undefToNull = <A>(a: A | undefined): Nullable<A> =>
  a === undefined ? null : a;
