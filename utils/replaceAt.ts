export type ReplaceAt = (
    index: number,
    chr: string,
) => (str: string) => string;

export const replaceAt: ReplaceAt = (index, chr) => (str): string => {
    if (index > str.length - 1) return str;
    return str.substring(0, index) + chr + str.substring(index + chr.length);
};
