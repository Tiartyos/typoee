import {removeManyAt} from './remove-many-at';

interface RemoveAt1Fn {
    <T>(xs: Array<T>): Array<T>;

    (xs: string): string;
}

/**
 * Remove one item/character after first {@param n} items/characters.
 *
 * @example
 * ```ts
 * removeAt(1)([0, 1, 2]) // [0, 2]
 * removeAt(2)('abc') // 'ab'
 * ```
 *
 * @param n
 */
export const removeAt: (n: number) => RemoveAt1Fn = n => removeManyAt(n)(1);
