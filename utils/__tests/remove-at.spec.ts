/* eslint-disable @typescript-eslint/no-unused-vars */
import {expect} from 'chai';

import {removeAt} from '../remove-at';

describe('removeAt', () => {
    it('removes at given position from array', () => {
        expect(removeAt(0)([0, 1, 2])).to.eql([1, 2]);
        expect(removeAt(1)([0, 1, 2])).to.eql([0, 2]);
        expect(removeAt(2)([0, 1, 2])).to.eql([0, 1]);
        // @ts-expect-error array should not produce string
        const y: string = removeAt(0)([]);
    });
    it('removes at given position from string', () => {
        expect(removeAt(0)('abc')).to.eql('bc');
        expect(removeAt(1)('abc')).to.eql('ac');
        expect(removeAt(2)('abc')).to.eql('ab');
        // @ts-expect-error string should not produce array
        const y: Array<unknown> = removeAt(0)('');
    });
});
