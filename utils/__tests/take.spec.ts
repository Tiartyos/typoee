/* eslint-disable @typescript-eslint/no-unused-vars */
import {expect} from 'chai';

import {take} from '../take';

describe('take', () => {
    it('takes from array', () => {
        expect(take(0)([0, 1, 2])).to.eql([]);
        expect(take(2)([])).to.eql([]);
        expect(take(2)([0, 1, 2])).to.eql([0, 1]);
        // @ts-expect-error array should not produce string
        const y: string = take(2)([]);
    });
    it('takes from string', () => {
        expect(take(0)('abc')).to.eql('');
        expect(take(2)('')).to.eql('');
        expect(take(2)('abc')).to.eql('ab');
        // @ts-expect-error string should not produce array
        const y: Array<unknown> = take(2)('');
    });
});
