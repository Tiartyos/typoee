/* eslint-disable @typescript-eslint/no-unused-vars */
import {expect} from 'chai';

import {drop} from '../drop';

describe('drop', () => {
    it('drops from array', () => {
        expect(drop(0)([0, 1, 2])).to.eql([0, 1, 2]);
        expect(drop(2)([])).to.eql([]);
        expect(drop(2)([0, 1, 2])).to.eql([2]);
        // @ts-expect-error array should not produce string
        const y: string = drop(2)([]);
    });
    it('drops from string', () => {
        expect(drop(0)('abc')).to.eql('abc');
        expect(drop(2)('')).to.eql('');
        expect(drop(2)('abc')).to.eql('c');
        // @ts-expect-error string should not produce array
        const y: Array<unknown> = drop(2)('');
    });
});
