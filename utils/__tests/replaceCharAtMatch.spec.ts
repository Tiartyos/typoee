/* tslint:disable:no-unused-expression no-console */
/* eslint-disable no-irregular-whitespace */

import * as chai from 'chai';
import * as spies from 'chai-spies';
import {opt} from 'ts-opt';

import {replaceCharAtMatch, ReplacementOptions} from '../replaceCharAtMatch';

chai.use(spies);
const {expect} = chai;
chai.should();
// const sandbox = chai.spy.sandbox();

const text1 = 'asd test MATCHEDTEXT Test asdasd';
const regex1 = /(?<=[\p{L}])( | )+MATCHEDTEXT( | )+(?=[\p{L}])/u;
const matched1 = opt(text1.match(regex1)).orCrash('Invalid index');
const expectedRes1 = {
    error: {
        errorMessage: null,
        matchedText: ' MATCHEDTEXT ',
    },
    fixedValue: 'asd testXXXMATCHEDTEXT Test asdasd',
    location: {
        filePath: '',
        objectPath: 'test.objectPath',
        ruleFunctionName: 'CS.unit',
    },
    status: 'fixed',
};


const text2 = 'asd test MATCHEDTEXT Test asdasd';
const regex2 = /(?<=[\p{L}])( | )+MATCHEDTEXT( | )+(?=[\p{L}])/u;
const matched2 = opt(text1.match(regex2)).orCrash('Invalid index');
const expectedRes2 = {
    error: {
        errorMessage: null,
        matchedText: ' MATCHEDTEXT ',
    },
    fixedValue: 'asd test MATCHEDTEXT XXXest asdasd',
    location: {
        filePath: '',
        objectPath: 'test.objectPath',
        ruleFunctionName: 'CS.unit',
    },
    status: 'fixed',
};


const text3 = 'asd test.999.Test asdasd';
const regex3 = /(?<=[\p{L}]*[ | |.]{1})\d*(\.){1}(?=[\p{L}])/u;
const matched3 = opt(text3.match(regex3)).orCrash('Invalid index');
const expectedRes3 =
  {
      error: {
          errorMessage: null,
          matchedText: '999.',
      },
      fixedValue: 'asd test.999.XXXTest asdasd',
      location: {
          filePath: '',
          objectPath: 'test.objectPath',
          ruleFunctionName: 'CS.unit',
      },
      status: 'fixed',
  };


const conf1: ReplacementOptions = {
    replacementValue: ['XXX'],
    replacePosition: 'beginning',
    location: {
        filePath: '',
        objectPath: 'test.objectPath',
        ruleFunctionName: 'CS.unit',
    },
    firstMatched: matched1,
    value: text1,
    numberOfCharactersToReplace: 1,
};

const conf2: ReplacementOptions = {
    replacementValue: ['XXX'],
    replacePosition: 'end',
    location: {
        filePath: '',
        objectPath: 'test.objectPath',
        ruleFunctionName: 'CS.unit',
    },
    firstMatched: matched2,
    value: text2,
    numberOfCharactersToReplace: 1,
};


const conf3: ReplacementOptions = {
    replacementValue: ['XXX'],
    replacePosition: 'end',
    location: {
        filePath: '',
        objectPath: 'test.objectPath',
        ruleFunctionName: 'CS.unit',
    },
    firstMatched: matched3,
    value: text3,
    numberOfCharactersToReplace: 0,
};

describe('ReplaceFromTo', () => {
    it('replaces 1 character replacePosition: beginning', () => {
        expect(replaceCharAtMatch(conf1)).to.eql(expectedRes1);
    });
});

describe('ReplaceFromTo', () => {
    it('replaces 1 character replacePosition: end', () => {
        expect(replaceCharAtMatch(conf2)).to.eql(expectedRes2);
    });
});

describe('ReplaceFromTo', () => {
    it('inserts character replacePosition: end', () => {
        expect(replaceCharAtMatch(conf3)).to.eql(expectedRes3);
    });
});
