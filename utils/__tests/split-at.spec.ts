/* eslint-disable @typescript-eslint/no-unused-vars */
import {expect} from 'chai';

import {splitAt} from '../split-at';

describe('splitAt', () => {
    it('splits arrays', () => {
        expect(splitAt(1)([])).to.eql([[], []]);
        expect(splitAt(1)([0, 1, 2])).to.eql([[0], [1, 2]]);
        expect(splitAt(0)([0, 1, 2])).to.eql([[], [0, 1, 2]]);
        expect(splitAt(3)([0, 1, 2])).to.eql([[0, 1, 2], []]);
        expect(splitAt(10)([0, 1, 2])).to.eql([[0, 1, 2], []]);
        // @ts-expect-error array should not produce string
        const y: [string, string] = splitAt(1)([]);
    });
    it('splits strings', () => {
        expect(splitAt(1)('')).to.eql(['', '']);
        expect(splitAt(1)('abc')).to.eql(['a', 'bc']);
        expect(splitAt(0)('abc')).to.eql(['', 'abc']);
        expect(splitAt(3)('abc')).to.eql(['abc', '']);
        expect(splitAt(10)('abc')).to.eql(['abc', '']);
        // @ts-expect-error string should not produce array
        const y: [Array<unknown>, Array<unknown>] = splitAt(1)('');
    });
});
