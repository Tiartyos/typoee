/* eslint-disable @typescript-eslint/no-unused-vars */
import {expect} from 'chai';

import {insertElemsAt} from '../insert-elems-at';

describe('insertElemsAt', () => {
    it('inserts array into array', () => {
        expect(insertElemsAt(1)([])([])).to.eql([]);
        expect(insertElemsAt(1)([1, 3])([2])).to.eql([1, 2, 3]);
        expect(insertElemsAt(0)([1, 3])([2])).to.eql([2, 1, 3]);
        expect(insertElemsAt(10)([1, 3])([2])).to.eql([1, 3, 2]);
        // @ts-expect-error array should not produce string
        const y: string = insertElemsAt(0)([])([]);
    });
    it('inserts string into string', () => {
        expect(insertElemsAt(1)('')('')).to.eql('');
        expect(insertElemsAt(1)('ac')('b')).to.eql('abc');
        expect(insertElemsAt(0)('ac')('b')).to.eql('bac');
        expect(insertElemsAt(10)('ac')('b')).to.eql('acb');
        // @ts-expect-error string should not produce array
        const y: Array<unknown> = insertElemsAt(0)('')('');
    });
});
