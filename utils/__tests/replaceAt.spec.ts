/* tslint:disable:no-unused-expression no-console */
import * as chai from 'chai';
import * as spies from 'chai-spies';

import {replaceAt} from '../replaceAt';

chai.use(spies);
const {expect} = chai;
chai.should();
// const sandbox = chai.spy.sandbox();

const testText1 = '012345678';
const testText2 = '0123x5678';

describe('ReplaceAt', () => {
    it('replaces character at specified index', () => {
        expect(replaceAt(4, 'x')(testText1)).to.eql(testText2);
    });
});
