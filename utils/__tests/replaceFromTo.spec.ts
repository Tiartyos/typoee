/* tslint:disable:no-unused-expression no-console */
import * as chai from 'chai';
import * as spies from 'chai-spies';

import {replaceFromTo} from '../replaceFromTo';

chai.use(spies);
const {expect} = chai;
chai.should();
// const sandbox = chai.spy.sandbox();

const testText1 = 'asd_150 yxc';
const testText2 = 'asd -150 yxc';


describe('ReplaceFromTo', () => {
    it('replaces character at specified index', () => {
        expect(
            replaceFromTo(3, 4, ' -')(testText1)).to.eql(testText2);
    });
});


describe('ReplaceFromTo Negative indexes', () => {
    it('replaces character at specified index', () => {
        expect(
            replaceFromTo(4, 3, ' -')(testText1)).to.eql(testText2);
    });
});
