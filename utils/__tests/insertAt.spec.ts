/* tslint:disable:no-unused-expression no-console */
import * as chai from 'chai';
import * as spies from 'chai-spies';

import {insertAt} from '../insertAt';

chai.use(spies);
const {expect} = chai;
chai.should();
// const sandbox = chai.spy.sandbox();

const testText1 = '012345678';
const testText2 = '0123xxx45678';

describe('ReplaceAt', () => {
    it('insterts string at specified index', () => {
        expect(insertAt(4, 'xxx')(testText1)).to.eql(testText2);
    });
});
