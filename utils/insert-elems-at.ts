import {pipe} from 'ts-opt';
import {isString, join, flatten} from 'lodash/fp';

import {splitAt} from './split-at';

interface InsertElemsAt1Fn {
    <T>(source: Array<T>): (toInsert: Array<T>) => Array<T>;

    (source: string): (toInsert: string) => string;
}

/**
 * Insert `toInsert` array/string into a source array/string after first {@param n} elements.
 *
 * @example
 * ```ts
 * insertElemsAt(1)([1, 3])([2]) // [1, 2, 3]
 * insertElemsAt(1)('ac')('b') // 'abc'
 * ```
 *
 * @param n
 */
export const insertElemsAt: (n: number) => InsertElemsAt1Fn =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (n: number) => (source: any) => (toInsert: any): any => {
        const [prefix, suffix] = splitAt(n)(source);

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return pipe([prefix, toInsert, suffix], isString(source) ? join('') : flatten as any);
    };
