/* eslint-disable no-console */

import {fold} from 'fp-ts/lib/Either';
import {pipe} from 'fp-ts/lib/pipeable';
import * as t from 'io-ts';
import {failure} from 'io-ts/lib/PathReporter';

const space = 3;

function getErrorMessage(errors: t.Errors): string {
    const description = failure(errors).join('\n');

    return `Validation error;\n${description}`;
}

function logError(value: unknown, errors: t.Errors): void {
    console.error(
        'validation error: ',
        JSON.stringify(value, null, space),
        '\n',
        JSON.stringify(errors, null, space),
    );
}

export const validateSchema = <A>(type: t.Type<A>)=>
    (value: unknown)=> (errorMessage: string) : A =>
        pipe(
            type.decode(value),
            fold(
                (errors: t.Errors) => {
                    console.log(errorMessage);
                    logError(value, errors);
                    throw new Error(getErrorMessage(errors));
                },
                () => value as A,
            ),
        );
