export type InsertAt = (
    index: number,
    chr: string,
) => (str: string) => string;

export const insertAt: InsertAt = (index, chr) => (str): string => {
    if (index > str.length - 1) return str;

    return str.substring(0, index) + chr + str.substring(index);
};
