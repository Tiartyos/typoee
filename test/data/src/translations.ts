/* eslint-disable max-lines */

export const testTranslations = {
  CS: {
    'test': {
      someItem: 'some text k CS.5.lol',
      someNode: {
        'insideSomeNode':1,
        'insideSomeNode2':2,
        'insideSomeNodeObj':{
          insideSomeNodeObj: 'test',
          uglyIgnoredKey: 'aAbnMasda.5.5.fasfqwewq'
        },
      }
    },
  },
  EN: {
    'test': {
      someItem: 'some text EN',
      someNode: {
        'insideSomeNode':1,
        'insideSomeNode2':2,
      'insideSomeNodeObj':{
        insideSomeNodeObj: 'test',
        uglyIgnoredKey: 'aAbnMasda.5.5.fasfqwewq'
      },
      }
    },
  }
}
