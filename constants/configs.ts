import {ConfigurationRequiredWithNulls} from '../types/ConfigRequiredWithNulls';

export const defaultConfig: ConfigurationRequiredWithNulls = {
    quiet: true,
    ignoreKeys: {CS: [], EN: []},
    crawler: {
        rootPath: null,
        fileNameRegex: 'translations.ts',
    },
    verbose: false,
    fixIssues: false,
    ignoreLanguages: ['EN'],
    ignoreRules: {CS: {}, EN: {}},
    files: null,
    disableUnicodeFix: false,
};
