import * as t from 'io-ts';

import {excess} from '../utils/excess';
import {CrawlerRequiredSchema} from './CrawlerRequiredWithNulls';
import {LanguageSchema} from './Language';
import {LanguageKeysSchema} from './LanguageKeys';
import {LanguagesSchema} from './Languages';

// Configuration file description

const optionalPart = t.interface({
    crawler: t.union([CrawlerRequiredSchema, t.null]),
    fixIssues: t.union([t.boolean, t.null]),
    quiet: t.union([t.boolean, t.null]),
    verbose: t.union([t.boolean, t.null]),
    ignoreRules: t.union([LanguagesSchema, t.null]),
    ignoreLanguages: t.union([t.array(LanguageSchema), t.null]),
    files: t.union([t.array(t.string), t.null]),
    ignoreKeys: t.union([LanguageKeysSchema, t.null]),
    disableUnicodeFix: t.union([t.boolean, t.null]),
});

export const ConfigurationRequiredWithNullsSchema = excess(optionalPart);

export interface ConfigurationRequiredWithNulls extends t.TypeOf<typeof ConfigurationRequiredWithNullsSchema> {}
