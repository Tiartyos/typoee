import * as t from 'io-ts';

import {excess} from '../utils/excess';

// Crawler settings description

const optionalPart = t.interface({
    fileNameRegex: t.union([t.string, t.null]),
    rootPath: t.union([t.string, t.null]),
});

export const CrawlerRequiredSchema = excess(optionalPart);

export interface CrawlerRequiredWithNulls extends t.TypeOf<typeof CrawlerRequiredSchema> {}
