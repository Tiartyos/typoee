import * as t from 'io-ts';
// Language description

export const LanguageSchema = t.keyof({
    CS: null,
    EN: null,
});


export type Language = t.TypeOf<typeof LanguageSchema>;
