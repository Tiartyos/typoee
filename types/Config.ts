import * as t from 'io-ts';

import {excess} from '../utils/excess';
import {CrawlerSchema} from './Crawler';
import {LanguageSchema} from './Language';
import {LanguageKeysSchema} from './LanguageKeys';
import {LanguagesSchema} from './Languages';

// Configuration file description

const optionalPart = t.partial({
    crawler: CrawlerSchema,
    fixIssues: t.boolean,
    quiet: t.boolean,
    verbose: t.boolean,
    ignoreRules: LanguagesSchema,
    ignoreLanguages: t.array(LanguageSchema),
    ignoreKeys: LanguageKeysSchema,
    files: t.array(t.string),
    disableUnicodeFix: t.boolean,
});

export const ConfigurationSchema = excess(optionalPart);

export interface Configuration extends t.TypeOf<typeof ConfigurationSchema> {}
