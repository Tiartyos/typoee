import * as t from 'io-ts';

import {excess} from '../utils/excess';

// Configuration file description

const optionalPart = t.partial({
    fileNameRegex: t.string,
    rootPath: t.string,
});

export const CrawlerSchema = excess(optionalPart);

export interface Crawler extends t.TypeOf<typeof CrawlerSchema> {}
