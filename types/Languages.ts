import * as t from 'io-ts';

import {excess} from '../utils/excess';
import {RulesSchema} from './RuleName';

// Languages description

const requiredPart = t.interface({
    CS: RulesSchema,
    EN: RulesSchema,
});

export const LanguagesSchema = excess(requiredPart);

export interface Languages extends t.TypeOf<typeof LanguagesSchema> {}
