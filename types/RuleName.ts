import * as t from 'io-ts';

import {excess} from '../utils/excess';

// Rules description

const optionalPart = t.partial({
    preposition: t.boolean,
    conjunction: t.boolean,
    abbreviation: t.boolean,
    unit: t.boolean,
    hyphen: t.boolean,
    ellipsis: t.boolean,
    ordinalNumeral: t.boolean,
    quoteMark: t.boolean,
});

export const RulesSchema = excess(optionalPart);

export interface Rules extends t.TypeOf<typeof RulesSchema> {}
