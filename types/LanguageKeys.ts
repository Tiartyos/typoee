import * as t from 'io-ts';

import {excess} from '../utils/excess';

// Languages description

const requiredPart = t.interface({
    CS: t.array(t.string),
    EN: t.array(t.string),
});

export const LanguageKeysSchema = excess(requiredPart);

export interface LanguageKeys extends t.TypeOf<typeof LanguageKeysSchema> {}
